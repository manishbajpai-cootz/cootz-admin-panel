const express = require("express");

const router = express.Router();

const { winner } = require("../../controllers/apis/winners.js");

// router.get("/joinContest", joinContest);
// router.post("/createContest", createContest);
router.post("/winner", winner);
module.exports = router;
