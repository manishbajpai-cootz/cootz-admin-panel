const express = require("express");
const router = express.Router();
const auth = require("../../middlewares/auth");

const {
    createcontest,
    createcontestAdmin,
    joincontest,
    getcontest,
    getcontestquestiontype,
    getsplash,
    gethomescreencontest,
    market,
    pocket,
    buynsell,
    userresult,
    winnerlist,
    getallcontestType,
    getcontestquestion,
    allcontest, getallcontests, editcontest, userscontest, withdrawal, wallets,
    updatewallets
} = require("../../controllers/apis/contest.js");

router.post("/createcontest", auth, createcontest);
router.post("/createcontestAdmin", createcontestAdmin);
router.post("/joincontest", auth, joincontest);
router.get("/getcontest/:contestId", auth, getcontest);
router.get("/getcontestquestiontype/:contestId", auth, getcontestquestiontype);
router.get("/getsplash", getsplash);
router.get("/home", auth, gethomescreencontest)
router.get("/market", auth, market);
router.get("/pocket", auth, pocket);
router.post("/buynsell", auth, buynsell);
router.post("/userresult", auth, userresult)
router.post("/winnerlist", auth, winnerlist)
router.get("/getallcontestType", auth, getallcontestType)
router.get("/getallcontest", auth, allcontest)
router.get("/getallcontests", getallcontests)
router.put("/editcontest", editcontest)
router.get("/getcontestquestion/:contestId", getcontestquestion)
router.get("/userscontest", auth, userscontest);
router.get("/withdrawal/:amount", auth, withdrawal);
router.get("/wallet", auth, wallets);
router.put("/updatewallets", auth, updatewallets)




module.exports = router;