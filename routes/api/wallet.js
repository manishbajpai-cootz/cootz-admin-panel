const express = require("express");

const router = express.Router();
const auth = require("../../middlewares/auth");
const { winnerAmount, updatewallets } = require("../../controllers/apis/contest.js");

router.get("/winnerAmount", winnerAmount);
router.get("/wallet", wallet);

module.exports = router;
