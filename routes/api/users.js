// import express from "express";
const express = require("express");
const router = express.Router();
const auth = require("../../middlewares/auth");

const {
  signin,
  signup,
  logout,
  addDetails,
  forgotPassword,
  existingUser,
  check,
  ticket,
  follower,
  userdetails,
  follwerdetails,
  sendotp,
  verifyotp
} = require("../../controllers/apis/users.js");

router.post("/signin", signin);
router.post("/signup", signup);
router.get("/logout", logout);
router.put("/addDetails", auth,addDetails);
router.put("/forgotPassword",forgotPassword);
router.get("/existingUser", existingUser);
router.get("/test", auth, check);
router.put("/follower",auth,follower);
router.get('/userdetails',auth,userdetails);
router.post("/ticket",ticket);
router.post("/verifyotp", verifyotp);
router.get("/follwerdetails",auth,follwerdetails)

module.exports = router;
