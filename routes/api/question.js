const express = require("express");
const router = express.Router();
const auth = require("../../middlewares/auth");

const {
    addquestion,
    getques
    
    

} = require("../../controllers/apis/question.js");

router.post("/addquestion", addquestion);
router.get("/getques", getques);




module.exports = router;