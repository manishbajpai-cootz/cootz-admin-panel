import express from "express";
import { home } from "../../controllers/apis/home";
const router = express.Router();
router.use("/", home);

module.exports = router;