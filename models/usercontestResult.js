const mongoose = require("mongoose");

const usercontestResultSchema = new mongoose.Schema({


   contestId: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "contest",
   }],
   userresponse: [{}],
   userId: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
   }],
   score: Number,
   rank: String,




}, { timestamps: true })


module.exports = mongoose.model("userresult", usercontestResultSchema);