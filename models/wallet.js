const mongoose = require("mongoose");

const walletSchema = new mongoose.Schema({
  winningCash: Number,
  bonusCash: Number,
  depositCash: Number,
  userId: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  }],
});

module.exports = mongoose.model("WalletMoney", walletSchema);
