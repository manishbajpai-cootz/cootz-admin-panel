const mongoose = require("mongoose");

//Schema to be changed as desired
//export to controllers/users
const followerSchema = new mongoose.Schema(
  {
   //jo humko follow kr raha hai
   follower:[{
       type:mongoose.Schema.Types.ObjectId,
       ref:"User"
   }],
   // jisko hum follow kr rahe hai
    following:[{
        type:mongoose.Schema.Types.ObjectId,
        ref:"User"
    }],
   userId:[{
    type:mongoose.Schema.Types.ObjectId,
    ref:"User"
   }]
    
 
  
   
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("follower", followerSchema);