const mongoose = require("mongoose");

const Question = new mongoose.Schema(
  {
    questions: String,
    option: [{
      optiontext: String,
      MediaUrl: String,
      optionNumber: Number,
    }],
    CorrectOption: {
      type: Number,
    },
    CorrectAnswerExplanation: String,
    CorrectAnswerMediaUrl: String,
    chapterName: String,
    SubjectName: String,

    questiontype: {
      type: String,
      enum: ["MCQ", "MTOC"],
      default: "MCQ"
    },

    contestId: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "contest"
    }]
  },

  { timestamps: true }
);

// let homePage = mongoose.model("HomeSchema", HomeSchema);
module.exports = mongoose.model("Question", Question);