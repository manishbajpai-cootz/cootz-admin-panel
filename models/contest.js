const mongoose = require("mongoose");

const contest = new mongoose.Schema(
  {
    contestName: String,
    constestType: {
      type: String,
      enum: ["Exam", "Trivia"]
    },
    contestsubType: {
      type: String
    },
    sponsered: {
      type: String,
      enum: ["Yes", "No"],
      default: "No"
    },

    intialentryFee: Number,
    entryFee: Number,
    feehistory: [{}],
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    rank: [String],
    currentslot: Number,
    players: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    }],
    playerScore: [Number],
    questions: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "Question"
    }],
    totalPlayers: Number,
    intialwinningamount: {
      type: Number,
    },
    winningamount: {
      type: Number,
    },
    winnerBoard: [{}],
    winners: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    }],
    status: {
      type: String,
      enum: ['Finished', 'Created', 'Canceled', 'Onapp', "Live", "Inreview", "Ended"],
      default: "Created"
    },
    // Date format will be DD-MM-YYYY HH:mm:ss
    startdate: {

    },
    enddate:
    {

    },
    sellType: Boolean,
    createdBy: {
      type: mongoose.Schema.Types.ObjectId
    },
    TotalTimeInMinute: {
      type: Number
    },
    totalquestion: {
      type: Number
    },
    img: {
      type: String
    }
  },
  { timestamps: true }
);

// user data model

// let JoinContest = mongoose.model("JoinContest", contestSchema);
module.exports = mongoose.model("contest", contest);

// export default JoinContest;
