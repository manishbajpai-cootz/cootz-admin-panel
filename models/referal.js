const mongoose = require("mongoose");

const referalSchema = new mongoose.Schema(
  {
    code: String,
    codeType: {
      type: String,
      enum: ["ByUser", "ByMarketing"],
      default: "ByUser",
    },

    entryFee: String,
    creatorId: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    }],
    CodeLimit: {
      type: Number,
    },
    usedLimit: {
      type: Number,
    },
    usedUserId: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    }],
    rule: {},
  },
  { timestamps: true }
);

// user data model

// let JoinContest = mongoose.model("JoinContest", contestSchema);
module.exports = mongoose.model("referal", referalSchema);

// export default JoinContest;

/*
COde should tell us the rule
- UserId if refreal code it should contain the amount which will be crewdited to both 
rule for creadit

*/
