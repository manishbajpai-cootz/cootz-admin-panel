const mongoose = require("mongoose");

//Schema to be changed as desired
//export to controllers/users
const userSchema = new mongoose.Schema(
  {
    email: { type: String },
    username: { type: String },
    lastname: { type: String },
    password: { type: String },
    status: {
      type: String,
      enum: ["Active", "InActive"],
      default: ["Active"]
    },
    signInType: {
      type: String,
      enum: ["fb", "google", "byphone", "PreLaunch"],
      default: "byphone",
    },
    phoneNumber: { type: String },
    walletAmount: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "WalletMoney"
    }],
    photo: {
      type: String
    },
    userProfile: {
      type: String
    },
    contests: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "JoinContest"
    },
    lastlogin: String,
    firstlogin: mongoose.Schema.Types.Date,
    intrest: {
      type: [String]
    },
    ReferalCode: {
      type: mongoose.Schema.Types.ObjectId, ref: "referal"
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("User", userSchema);
