const mongoose = require("mongoose");
const quesdbconn = require('../app');


const Questions = new mongoose.Schema(
  {

    question: String,
    questionImg: [],
    questiontype: {
      type: String,
    },
    option1: [],
    option2: [],
    option3: [],
    option4: [],
    exam: [],
    Level: String,
    chapter: String,
    subject: String,
  },

  { timestamps: true }
);

// let homePage = mongoose.model("HomeSchema", HomeSchema);
module.exports = quesdbconn.model("questions", Questions);