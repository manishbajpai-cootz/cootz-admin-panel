const mongoose = require("mongoose");

const buynsellSchema = new mongoose.Schema(
  {
    paymentType: String,

    amount: String,
    contestId: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "contest",
    }],
    TranscationType: {
      type: String,
      enum: ["Buy", "Sell"],
      default: "Sell"
    },
    paymentId: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "Payment",
    }],
    userId: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    }],

  },
  { timestamps: true }
);

// user data model

// let JoinContest = mongoose.model("JoinContest", contestSchema);
module.exports = mongoose.model("usercontest", buynsellSchema);