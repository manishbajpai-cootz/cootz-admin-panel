const mongoose = require("mongoose");

//Schema to be changed as desired
//export to controllers/users
const winnerSchema = new mongoose.Schema({

  contestId: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "contest"
  }],
  rank: [{}]

});

module.exports = mongoose.model("Winner", winnerSchema);
// export default mongoose.model("Winner", userSchema);
