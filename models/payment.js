const mongoose = require("mongoose");

const paymentSchema = new mongoose.Schema(
  {
    paymentType: [ {} ],

    amount: String,
    userId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    status: {
      type: String,
      enum: ["Pending", "Successfull", "Reject"],
    },
    statusbymerchant: {
      type: String,
    },
  },
  { timestamps: true }
);

// user data model

// let JoinContest = mongoose.model("JoinContest", contestSchema);
module.exports = mongoose.model("Payment", paymentSchema);
