const express = require("express");
const BodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const cookieparser = require("cookie-parser");
const user = require("./routes/api/users");
const winners = require("./routes/api/winners");

require("dotenv").config();
//const route=require("./routes/routes");
const path = require("path");
//const mg =require("./models/message");
const moment = require("moment");
// import env from "./config/environment.js";
const { development, production } = require("./config/environment");

const app = express();
app.use(express.json());
app.use(BodyParser.json({ limit: "10mb", extended: true }));
app.use(BodyParser.urlencoded({ limit: "10mb", extended: true }));
app.use(cookieparser());
app.use(cors({ credentials: true, Origin: "*" }));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// const authRouter = require('./routes/authRouter');


// const PORT = env.development.PORT;
const DATABASE_URL = development.MONGODB_URL;
// const DATABASE_URL = production.MONGODB_URL;
const MONGODB_QES = development.MONGODB_QES;
console.log("🚀 ~ file: app.js ~ line 38 ~ MONGODB_QES", MONGODB_QES)
console.log(development["MONGODB_URL"]);

// "mongodb+srv://helloworld:helloworld123@cluster0.qjkjd.mongodb.net/db1_ms?retryWrites=true&w=majority";
// "mongodb+srv://thymaster:thymaster123@cluster0.qjkjd.mongodb.net/cootz?retryWrites=true&w=majority";

const PORT = process.env.PORT || 5000; //setup PORT in .env file before proceeding.

mongoose
  // .connect(DATABASE_URL, {
  .connect(DATABASE_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .then(() => {
    console.log("DB CONNECTED SUCCESSFULLY");
  })
  .catch((err) => {
    console.log("PROBLEM CONNECTING DB", err);
  });

const quesdbconn = mongoose.createConnection(MONGODB_QES, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
})

quesdbconn.then((quesdbconn) => {
  console.log(quesdbconn._readyState == 1 ? 'quesdbconn DB CONNECTED SUCCESSFULLY' : 'PROBLEM CONNECTING Cost DB')
})

module.exports = quesdbconn;

const server = app.listen(PORT, () => {
  console.log(`Port is running at ${PORT}`);
});


const userRouter = require("./routes/api/users");

//  const contestRouter = require("./routes/api/wallet");
const winnerRouter = require("./routes/api/winners");
const contestRouter = require("./routes/api/contest");
const questionRouter = require("./routes/api/question");
// const taskRouter = require('./routes/taskRouter');
// const folderRouter = require('./routes/folderRouter');
// routes

// app.use(authRouter);
app.use(userRouter);

app.use(winners);
app.use(winnerRouter);
app.use(contestRouter);
app.use(questionRouter)
app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

// error handler middleware
app.use((error, req, res, next) => {
  console.log("error", error);
  res.status(error.status || 500).send({
    error: {
      status: error.status || 500,
      message: error.message || 'Internal Server Error',
    },
  });
});