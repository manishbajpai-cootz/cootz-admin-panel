const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
// const mongoose = require("mongoose");
const UserModal = require("../../models/user.js");
const wallet = require("../../models/wallet");
const follow = require("../../models/folllower");
const asyncHandler = require('express-async-handler');
const refreal = require("../../models/referal")
const nodemailer = require('nodemailer');
const { randomUUID } = require("crypto");
const reqi = require("request");
const admin = require("firebase-admin");
const usercontestResult = require("../../models/usercontestResult")
// const wallet = require("../../models/wallet");
var request = require('sync-request');
//{signin} and {signup} to be exported to routes/user

const secret = "Mahadev";

// for checking whether the returning users credentials are valid or not

exports.signin = asyncHandler(async (req, res, next) => {
  console.log("login")
  let mobileNumber = req.body.mobileNumber;
  let email = req.body.email;
  let r = true
  if (mobileNumber) {
    r = false
    UserModal.findOne({ phoneNumber: req.body.mobileNumber, status: "Active" }, async function (err, user) {
      if (err) return res.status(500).send('Error on the server.');
      console.log("user", typeof (user));
      if (!user) return res.status(404).send('No user found.');
      // console.log("user",user)
      let r = await sendotp(mobileNumber);
      if (user) {
        console.log("inside user length if", user)
        // let passwordIsValid = bcrypt.compareSync(user.password);
        // console.log("passwordIsValid", passwordIsValid)
        // if (!passwordIsValid) return res.status(401).send({ auth: false, token: null });

        // let token = jwt.sign({ id: user._id }, secret, {
        //   expiresIn: '365d' // expires in 24 hours
        // });
        // console.log("token", token);
        // let wallets = await wallet.findOne({ userId: user._id })
        res.status(200).send({});
      }

    });
  }

});

exports.existingUser = async (req, res) => {
  const { email, signInType } = req.body;
  try {
    const returningUser = await UserModal.findOne({ email });
    const userType = await UserModal.findOne({ signInType });

    if (!returningUser)
      return res.status(404).json({ message: "User not found" });

    if (returningUser) {
      //for checking the signInType of phone registered users
      if (userType.signInType !== "phone") {
        return res
          .status(400)
          .json({ success: false, message: "User not registered using phone" });
      }
    }

    res.status(200).json({ message: "redirecting to reset password" });
  } catch (err) {
    res.status(400).json({ message: err });
    console.log(err);
  }
};

// for checking first time users are registered or not
exports.signup = asyncHandler(async (req, res, next) => {
  let phoneNumber = req.body.mobileNumber;
  let email = req.body.email;
  let password = req.body.password;
  let signInType = req.body.signInType;
  let username = req.body.username
  let hashedPassword = bcrypt.hashSync(phoneNumber, 8);
  const user = await UserModal.find({
    phoneNumber: phoneNumber,

  });
  //  console.log("user",user[0].status,user.length);
  if (user.length > 0) {
    console.log("user.length")
    if (user[0].status === "Active") {
      console.log("userstatus if", user)
      return res.status(201).send({ "msg": "user already exist with This Mobile number" })
    }
    if (user[0].status === "InActive") {
      let u = await UserModal.findOneAndUpdate({ phoneNumber: phoneNumber }, { status: "Active" }, { new: true });
      if (u) {
        let token = jwt.sign({ id: u._id }, secret, {
          expiresIn: '365d' // expires in 24 hours

        });
        let f = await follow.create({
          userId: u._id
        })
        console.log("follower", f)
        let refrealcode = await refrealCode(u._id)
        console.log("refrealcode", refrealcode)
        await updatewallet(u._id)
        let r = await sendotp(phoneNumber);
        // console.log("r", r["Response"])
        return res.status(200).send({ auth: true, refrealCode: refrealcode, msg91res: r["Response"] });

      }
    }
  } else {
    UserModal.create({
      phoneNumber: phoneNumber,
      email: email,
      username: username,
      password: hashedPassword,
      signInType: signInType,
      status: "Active",
    }).then(async (s, err) => {
      if (err) {
        return res.status(500).send({ "err occured while creating user": err })
      }
      //   console.log("secret",secret);
      if (s) {
        let token = jwt.sign({ id: s._id }, secret, {
          expiresIn: '365d' // expires in 24 hours
        });
        let f = await follow.create({
          userId: s._id
        })
        console.log("follower", f)
        let refrealcode = await refrealCode(s._id)
        console.log("refrealcode", refrealcode)
        await updatewallet(s._id)
        let r = await sendotp(phoneNumber);
        // console.log("r", r)
        return res.status(200).send({
          auth: true, token: token, user: s, refrealCode: refrealcode,
        })
      }
    })
  }
})
// router.post('/logout', function(req, res) {
exports.logout = async (req, res) => {
  //mock logout
  try {
    res.status(200).json({
      message: "user logged out",
    });
  } catch (err) {
    res.status(400).json({
      message: "something went wrong",
    });
    console.log(err);
  }
};

exports.forgotPassword = asyncHandler(async (req, res, next) => {
  const { password, mobileNumber } = req.body;

  if (mobileNumber && password) {
    const userEmail = await UserModal.findOne({ phoneNumber: mobileNumber });
    if (userEmail) {
      let r = await sendotp(phoneNumber);
      const changedPassword = await bcrypt.hash(newPassword, 12);
      await userEmail.updateOne({
        password: changedPassword,
      });
      // let r = await sendotp(mobileNumber);
      return res.status(200).send({ "password successfully updated": userEmail });
    }
  } else {
    return res.status(400).json({
      message: "Email Id or Password wrong",
    });
  }

});

// for storing user login data to db we are also saving user intrest by this api
exports.addDetails = asyncHandler(async (req, res, next) => {
  let media = req.body.photo;
  let intrest = req.body.intrest;
  let userProfile = req.body.userProfile
  let username = req.body.username
  console.log(req.user);
  // const newDetails = new UserModal({
  //   ...details,
  // });

  if (intrest) {

    let d = await UserModal.findOneAndUpdate({ _id: req.user._id }, { $push: { intrest: { $each: intrest } } })
  }
  if (media) {
    await UserModal.findOneAndUpdate({ _id: req.user._id },
      { $push: { photo: { $each: media }, userProfile: userProfile } }, { new: true })
  }
  console.log("🚀 ~ file: users.js ~ line 222 ~ exports.addDetails=asyncHandler ~ userProfile", userProfile)
  if (userProfile) {
    console.log("🚀 ~ file: users.js ~ line 222 ~ exports.addDetails=asyncHandler ~ userProfile", userProfile)
    let d = await UserModal.findOneAndUpdate({ _id: req.user._id },
      { userProfile: userProfile, username: username }, { new: true })
    console.log("🚀 ~ file: users.js ~ line 216 ~ exports.addDetails=asyncHandler ~ d", d)
  }

  res.status(200).json({ message: "Details added", "user": req.user });


});
exports.check = async (req, res, next) => {
  res.send("tested0");
};
exports.ticket = asyncHandler(async (req, res, next) => {
  let email = req.body.email
  let phoneNumber = req.body.phonenumber
  let firstname = req.body.firstname
  let lastname = req.body.lastname
  let ReferalCode = req.body.ReferalCode

  let t = await UserModal.findOne({ email: email })
  console.log("ti", t)
  if (t) {
    return res.status(401).send("already registered")
  }
  let ti = await UserModal.findOne({ phoneNumber: phoneNumber })
  console.log("ti", ti)
  if (ti) {
    return res.status(401).send("already registered")
  }
  if (ti == null && t == null) {

    let r = await UserModal.create({
      email: email,
      phoneNumber: phoneNumber,
      username: firstname,
      lastname: lastname,
      ReferalCode: ReferalCode,
      status: "InActive",
      signInType: "PreLaunch"
    })
    if (r) {
      sendemail(email)
      return res.status(200).send("user create")
    }
  }
})

exports.follower = asyncHandler(async (req, res, next) => {
  let id = req.user._id;
  let userIds = req.body.id;

  let r = await follow.findOneAndUpdate({ userId: id }, { $addToSet: { following: userIds } }, { new: true })
  console.log("r", r);
  if (r) {
    await follow.findOneAndUpdate({ userId: userIds[0] }, { $addToSet: { follower: id } }, { new: true })
    return res.status(200).send(r);
  }

})
exports.follwerdetails = asyncHandler(async (req, res, next) => {
  let userId = req.user._id;
  let r = await follow.find({ userId: userId }).populate('following')
  if (r) {
    return res.status(200).send(r)
  }
})
exports.userdetails = asyncHandler(async (req, res, next) => {
  let id = req.user.id
  let r = await follow.find({ userId: id }).populate('following').populate('follower')
  console.log("r", r);
  let f = (r[0].following).length;
  let f1 = (r[0].follower).length;
  console.log(f, f1);
  let refrealcode = await refreal.findOne({ creatorId: id })
  let wallets = await wallet.findOne({ userId: req.user._id })
  let usercontest = await usercontestResult.find({ userId: req.user._id }).populate('contestId')
  return res.status(200).send({ 'data': r[0], "refrealcode": refrealcode?.code, "wallet": wallets, user: req.user, "usercontest": usercontest })
})
exports.getfix = asyncHandler(async (req, res, next) => {
  let r = {
    "img": "",

  }
})

exports.tokenregister = asyncHandler(async (req, res, next) => {
  let userId = req.user._id;
  console.log("userId", userId);
  let token = req.body.token;
  await Device.find({ UserId: userId }).then(async (s, err) => {
    // console.log("token",s[0]["token"]);
    if (s.length > 0) {
      let r = s[0]["token"];
      // let u =  r.includes(token);


      let t = await Device.findOneAndUpdate({ UserId: userId }, { token: token }, { new: true })
      console.log("t", t);
      return res.status(200).send(t)
    }

    else {
      Device.create({
        token: req.body.token,
        UserId: userId

      }).then((es, err) => {
        if (err) { return res.status(400).send(err) }
        if (es) { return res.status(200).send(es) }
      })
    }
  })

})

exports.sendPush = asyncHandler(async (req, res, next) => {
  let token = req.body.token;
  console.log("t", token.length);
  await admin.messaging().sendMulticast({
    tokens: token,
    notification: {
      title: "cootz",
      body: "this is body test for today ",
      imageUrl: "http://res.cloudinary.com/IMG-20210715-WA0010_vt6qjx.jpg"
    },
  }).then((s) => {
    if (s) {
      console.log("S", s);
      res.status(200).json({ message: "Successfully sent notifications!" });
    }
  })
})






/*-----------------------------------------------------------Functions----------------------------------------------------------------------------------------------------------------------*/
function sendemail(email) {



  var transporter = nodemailer.createTransport({
    service: 'gmail',
    // port: 465,
    secure: false,
    auth: {
      user: 'connectwithus@cootz.in',
      pass: 'Hritwik@14'
    }
  });

  var mailOptions = {
    from: 'connectwithus@cootz.in',
    to: email,
    subject: 'Thanks You for signing up',
    text: 'Welcome to the cootz family'
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });
}

async function refrealCode(userId) {
  let code = randomUUID();
  console.log("code", code);
  await refreal.create({
    creatorId: userId,
    code: code

  }).then((s, err) => {
    if (s) {
      console.log("S of refreal ", s)
      code = s["code"]
    }
  })
  return code
}

async function updatewallet(userId) {
  await wallet.create({
    userId: userId,
    bonusCash: 20,
    depositCash: 0,
    winningCash: 0
  }).then((s, err) => {
    if (s) {
      return true
    } else {
      return false
    }
  })
}


async function sendotp(mobileNumber) {
  let mb = mobileNumber;
  let number = 91 + mb;
  console.log("number", number);
  let jstring;

  let r = await reqi({
    method: 'GET',
    url: ' https://api.msg91.com/api/v5/otp?template_id=618fd765f120af71e2048712&mobile=' + number + '&authkey=368305Acbdixi3sS6160109aP1&otp_length=5',
    // url: 'http://2factor.in/API/V1/a52db2e4-f294-11eb-8089-0200cd936041/SMS/' + mobileNumber + '/AUTOGEN',
    headers: { 'content-type': 'application/x-www-form-urlencoded' },

  }, function (error, resio, body) {
    if (error) {
      console.log(error);
    }
    if (!error && resio.statusCode == 200) {
      console.log("inside", body)
      let jstring = JSON.parse(body);
      console.log("body from msg91", jstring);
      // console.log("res.statusCode",resio)

    }
  })
  if (r) {
    console.log(jstring);
    return jstring;
  }

}
exports.verifyotp = asyncHandler(async (req, res, next) => {
  let otp = req.body.otp;
  let mb = req.body.mobileNumber;
  let jstring;
  //// find user and send token;;
  let number = 91 + mb;
  console.log("number", number);


  if (mb == "7905127546" && otp == '12345') {
    let l = await UserModal.findOneAndUpdate({ phoneNumber: mb }, { status: "Active" }, { new: true });
    console.log("🚀 ~ file: users.js ~ line 484 ~ exports.verifyotp=asyncHandler ~ l", l)
    let id = l._id;
    let token = jwt.sign({ id: l._id }, secret, {
      expiresIn: '365d' // expires in 24 hours

    });
    let wallets = await wallet.findOne({ userId: l._id })
    let refrealcode = await refrealCode(l._id)
    return res.status(200).send({ user: l, token: token, wallets: wallets, refrealcode: refrealcode })
  } else {

  var returnCode;
  var getUrl = "https://api.msg91.com/api/v5/otp/verify?authkey=368305Acbdixi3sS6160109aP1&mobile=" + number + '&otp=' + otp,


    returnCode = httpGet(getUrl);
  console.log("Status Code (main)     : " + returnCode);

  let u = JSON.parse(returnCode);
  console.log("u", u.type);

  if (u.type == "error") {
    return res.status(401).send(u['message'])
  }
  if (u.type == "success") {
    let l = await UserModal.findOneAndUpdate({ phoneNumber: mb }, { status: "Active" }, { new: true });
    console.log("🚀 ~ file: users.js ~ line 484 ~ exports.verifyotp=asyncHandler ~ l", l)
    let id = l._id;
    let token = jwt.sign({ id: l._id }, secret, {
      expiresIn: '365d' // expires in 24 hours

    });
    let wallets = await wallet.findOne({ userId: l._id })
    let refrealcode = await refrealCode(l._id)
    return res.status(200).send({ "message": u['message'], user: l, token: token, wallets: wallets, refrealcode: refrealcode })
  }
  }
  function httpGet(url) {
    var response = request(
      'GET',
      url
    );
    return response.body;
  }


})
/*
exports.verifyotp = asyncHandler(async (req, res, next) => {
  let mb = req.body.mobileNumber;
  let otp = req.body.otp
  let number = 91 + mb;
  console.log("number", number);
  reqi({
    method: 'GET',
    url: ' https://api.msg91.com/api/v5/otp/verify?authkey=368305Acbdixi3sS6160109aP1&mobile=' + number + '&otp=' + otp,
    // url: 'http://2factor.in/API/V1/a52db2e4-f294-11eb-8089-0200cd936041/SMS/' + mobileNumber + '/AUTOGEN',
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
  },
    function (error, resio, body) {
      if (error) {
        console.log(error);
        return error;
      }
      if (!error && resio.statusCode == 200) {
        console.log("inside", body)
        let jstring = JSON.parse(body)
        return res.send({ status: 200, msg: 'already verified' })
      }
    });
})

*/

async function addUsertotasksms(username, mobileNumber, taskname) {
  console.log("addusertotasksms", username, mobileNumber, taskname)
  let mb = 91 + mobileNumber
  let jsonDataObj = {
    "flow_id": "611a70b0a472393bfb5a3d76",
    "sender": "EnterSenderID",
    "mobiles": mb,
    "name": username,
    "taskname": taskname
  }
  reqi({
    method: 'POST',
    url: ' https://api.msg91.com/api/v5/flow/',
    headers: { 'content-type': 'application/x-www-form-urlencoded' },
    body: jsonDataObj,
    json: true

  }, function (error, resio, body) {
    if (error) {
      console.log(error);
    }
    if (!error && resio.statusCode == 200) {
      console.log("inside", body)
      let jstring = JSON.parse(body)
      // console.log("inside",typeof(jstring))
      console.log("inside", jstring)
      // console.log("inside",jstring["type"])
      // console.log("res.statusCode",resio)
      return jstring;
    }
  });
}