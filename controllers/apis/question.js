const asyncHandler = require('express-async-handler');
const contest = require("../../models/contest");
const user = require("../../models/user");
const question = require("../../models/question")
const questions = require("../../models/quesdbmodel")


exports.addquestion = asyncHandler(async (req, res, next) => {

    // getting the questions array from body
    const questionArray = req.body;

    // pushing all the questions in the array to the database
    questionArray.forEach(async (element) => {

        let questions = element.questions;
        let option = element.option;
        let chapterName = element.chapterName;
        let SubjectName = element.SubjectName;
        let contestId = element.contestId;
        let CorrectOption = element.CorrectOption

        // console.log("CorrectAnswer",CorrectAnswer);
        let r = await contest.findById(contestId);

        question.create({
            questions: questions,
            option: option,
            CorrectOption: CorrectOption,
            chapterName: chapterName,
            SubjectName: SubjectName,
            contestId: contestId
        }).then(async (s, err) => {
            if (s) {
                await contest.findOneAndUpdate({ _id: contestId }, { $push: { questions: s._id } })
                return res.status(200).send(s)
            }
        });
    })

})

exports.getques = asyncHandler(async (req, res, next) => {

    // console.log("CorrectAnswer",CorrectAnswer);

    let ques = await questions.find()
    console.log("🚀 ~ file: question.js ~ line 42 ~ exports.getques=asyncHandler ~ ques", ques)
    return res.status(200).send(ques)

})

