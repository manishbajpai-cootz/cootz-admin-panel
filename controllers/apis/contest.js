const UserModal = require("../../models/user.js");
const asyncHandler = require('express-async-handler');
const contest = require("../../models/contest");
const user = require("../../models/user");
const question = require("../../models/question")
const usercontest = require("../../models/usercontest")
const usercontestResult = require("../../models/usercontestResult");
const winner = require("../../models/winners")
const wallet = require("../../models/wallet")
const moment = require('moment')

exports.getallcontestType = asyncHandler(async (req, res, next) => {
    let d = [
        {
            "contestType": "JEE",
            "contestImg": "https://res.cloudinary.com/cootzs/image/upload/v1634115466/ticket1.1a5a267a_x7hkcw.png"
        },
        {
            "contestType": "SSC",
            "contestImg": "https://res.cloudinary.com/cootzs/image/upload/v1634115466/ticket1.1a5a267a_x7hkcw.png"
        },
        {
            "contestType": "UPSC",
            "contestImg": "https://res.cloudinary.com/cootzs/image/upload/v1634115466/ticket1.1a5a267a_x7hkcw.png"
        }, {
            "contestType": "NEET",
            "contestImg": "https://res.cloudinary.com/cootzs/image/upload/v1634115466/ticket1.1a5a267a_x7hkcw.png"
        }, {
            "contestType": "GATE",
            "contestImg": "https://res.cloudinary.com/cootzs/image/upload/v1634115466/ticket1.1a5a267a_x7hkcw.png"
        }, {
            "contestType": "UPTU",
            "contestImg": "https://res.cloudinary.com/cootzs/image/upload/v1634115466/ticket1.1a5a267a_x7hkcw.png"
        }, {
            "contestType": "SBI",
            "contestImg": "https://res.cloudinary.com/cootzs/image/upload/v1634115466/ticket1.1a5a267a_x7hkcw.png"
        }, {
            "contestType": "PO",
            "contestImg": "https://res.cloudinary.com/cootzs/image/upload/v1634115466/ticket1.1a5a267a_x7hkcw.png"
        }, {
            "contestType": "JRF",
            "contestImg": "https://res.cloudinary.com/cootzs/image/upload/v1634115466/ticket1.1a5a267a_x7hkcw.png"
        },
    ]

    res.status(200).send(d);
})


exports.updateintrest = asyncHandler(async (req, res, next) => {
    let id = req.user._id;
    let r = req.body.intrest;
    let d = await user.findOneAndUpdate({ "_id": id }, { $push: { intrest: { $each: r } } })
    return res.status(200).send(d)
})
exports.joincontest = asyncHandler(async (req, res, next) => {
    let contestId = req.body.contestId;
    let userId = req.user._id;

    let c = await contest.findById(contestId)
    let amount = parseInt(c['entryFee']);
    let lastentryfee = parseInt(c.entryFee)
    // console.log("lastentryfee", lastentryfee)
    let sellType = c.sellType
    if (c) {
        if (c.players.length > c.totalPlayers) {
            return res.status(401).send("Contest is full")
        } else {
            let r = false
            // let r = await payment(userId, amount, contestId)
            let d = await wallet.findOne({ userId: userId })
            if (d) {
                // console.log("wining cash before", d['winningCash'])
                if (lastentryfee <= d['winningCash']) {
                    let wc = d['winningCash'] - parseInt(lastentryfee)
                    // console.log("wc in first if", wc)
                    let firstif = await wallet.findOneAndUpdate({ userId: userId }, { winningCash: wc }, { new: true })
                    // console.log("updated winningCash in first if", firstif['winningCash'])
                    r = true
                }
                else if (d['winningCash'] < lastentryfee <= d['depositCash']) {
                    let remaining = lastentryfee - d['winningCash']
                    console.log("remaining", remaining);
                    let wc = d['depositCash'] - remaining
                    console.log("wc in second  if", wc)
                    let firstif = await wallet.findOneAndUpdate({ userId: userId }, { depositCash: wc, winningCash: 0 }, { new: true })
                    console.log("updated winningCash", firstif['winningCash'], firstif['depositCash'])
                    r = true
                } else {
                    return res.status(201).send("not sufficent amount to join")
                }
            }
            if (r == true) {

                let rd = await contest.findOneAndUpdate({ _id: contestId }, { $push: { players: userId } }, { new: true })

                // console.log("rd", rd);



                await usercontest.create({
                    amount: amount,
                    contestId: contestId,
                    TranscationType: "Buy",
                    userId: userId
                    // not adding payment Id as of now
                }).then((s, err) => {
                    // console.log("S", s);
                    if (sellType == true) {
                        changeentryfee(contestId)
                    }
                })
                return res.status(200).send("Be ready for Contest")
            }

        }


    }



})

exports.createcontest = asyncHandler(async (req, res, next) => {
    console.log("🚀 ~ file: contest.js ~ line 126 ~ exports.createcontest=asyncHandler ~ req.body.status", req.body.status)
    let contestName = req.body.contestName;
    let constestType = req.body.constestType;
    let contestsubType = req.body.contestsubType;
    let entryFee = req.body.entryFee;
    let totalPlayers = req.body.totalPlayers;
    let winningamount = req.body.winningamount;

    let startdate = req.body.startdate;
    let sellType = req.body.sellType
    let totalquestion = req.body.totalquestion;
    let createdBy = req.user._id;
    let TotalTimeInMinute = req.body.totaltime;
    let winnerBoard = await winningamounts(totalPlayers, winningamount)
    let enddate = req.body.enddate;
    let img = req.body.img;
    let sponsered = req.body.sponsered;


    contest.create({
        contestName: contestName,
        constestType: constestType,
        contestsubType: contestsubType,
        intialentryFee: entryFee,
        entryFee: entryFee,
        totalPlayers: totalPlayers,
        winnerBoard: winnerBoard,
        startdate: startdate,
        intialwinningamount: winningamount,
        winningamount: winningamount,
        enddate: enddate,
        createdBy: createdBy,
        sellType: sellType,
        totalquestion: totalquestion,
        TotalTimeInMinute: TotalTimeInMinute,
        intialwinningamount: winningamount,
        sponsered: sponsered,
        // status ko change krna hai
        status: req.body.status,
        img: img
    }).then((s, err) => {
        if (s) {
            return res.status(200).send(s)
        }
    })

})

exports.createcontestAdmin = asyncHandler(async (req, res, next) => {
    console.log("🚀 ~ file: contest.js ~ line 126 ~ exports.createcontest=asyncHandler ~ req.body.status", req.body.status)
    let contestName = req.body.contestName;
    let constestType = req.body.constestType;
    let contestsubType = req.body.contestsubType;
    let entryFee = req.body.entryFee;
    let totalPlayers = req.body.totalPlayers;
    let winningamount = req.body.winningamount;

    let startdate = req.body.startdate;
    let sellType = req.body.sellType
    let totalquestion = req.body.totalquestion;
    // let createdBy = req.user._id;
    let TotalTimeInMinute = req.body.totaltime;
    let winnerBoard = await winningamounts(totalPlayers, winningamount)
    let enddate = req.body.enddate;
    let img = req.body.img;
    let sponsered = req.body.sponsered;


    contest.create({
        contestName: contestName,
        constestType: constestType,
        contestsubType: contestsubType,
        intialentryFee: entryFee,
        entryFee: entryFee,
        totalPlayers: totalPlayers,
        winnerBoard: winnerBoard,
        startdate: startdate,
        intialwinningamount: winningamount,
        winningamount: winningamount,
        enddate: enddate,
        sellType: sellType,
        totalquestion: totalquestion,
        TotalTimeInMinute: TotalTimeInMinute,
        intialwinningamount: winningamount,
        sponsered: sponsered,
        // status ko change krna hai
        status: req.body.status,
        img: img
    }).then((s, err) => {
        if (s) {
            return res.status(200).send(s)
        }
    })

})

exports.getcontest = asyncHandler(async (req, res, next) => {

    let contestId = req.params.contestId;

    let s = await contest.findById(contestId).populate('players', 'username photo');
    if (s != null) {
        return res.status(200).send(s);
    }

})
exports.getcontestquestiontype = asyncHandler(async (req, res, next) => {

    let contestId = req.params.contestId
    console.log("contestId", contestId)
    let name = [];

    let r = await question.find({ contestId: contestId })
    console.log(r);
    if (r != null) {
        let m = {}
        r.map((e) => {
            m.chapterName = e.chapterName,
                m.SubjectName = e.SubjectName
            name.push(m)
            m = {}
        })
        return res.status(200).send(name)
    }
})

exports.gethomescreencontest = asyncHandler(async (req, res, next) => {
    let exam = []
    let Trivia = []
    let trending = []
    let cq = "178000.99"
    let pq = "23244243.0999"
    // let sponsered =[];
    let sponsered = []
    let r = await contest.find()
    if (r != null) {
        console.log("inside r", r.length);
        r.map(async (e) => {
            let currentdate = moment().utcOffset("+05:30").format()
            console.log("🚀 ~ file: contest.js ~ line 192 ~ r.map ~ currentdate", currentdate)
            let contestStartDate = moment(e.startdate).format()
            let contestEndDate = moment(e.enddate).format()
            console.log("diff of time contestStartDate", e.contestName, (moment(contestStartDate).diff(currentdate, 'hours')))
            console.log("diff of time contestEndDate", e.contestName, (moment(contestEndDate).diff(currentdate, 'hours')))
            if (e.status == "Onapp" || e.status == "Live" && moment(contestStartDate).diff(currentdate, 'seconds') == 0) {

                contest.findOneAndUpdate({ _id: e._id }, { status: 'Finished' }, { new: true })
            }
            if (e.status == "Onapp" && moment(contestEndDate).diff(currentdate, 'hours') < 2) {
                console.log("Live bano ")
                contest.findOneAndUpdate({ _id: e._id }, { status: 'Live' }, { new: true })
            }
            if (e.constestType === "Exam" && ((e.status == "Onapp"))) {
                // let s = e.players.includes(req.user._id)
                // intrest bano
                // if (s == false) {
                    exam.push(e)
                // }
            }
            if (e.constestType === "Trivia" && (e.status == "Onapp")) {
                // let s = e.players.includes(req.user._id)
                // if (s == false) {
                    Trivia.push(e)
                // }
            }
            if (e.status == "Live") {
                console.log("Live", e)
                // // let s = e.players.includes(req.user._id)
                // if (s == true) {
                let p = await usercontestResult.find({ contestId: e._id, userId: req.user._id })
                console.log("p", p.length)
                if (p.length <= 0) {
                    console.log("e in Live/trending", e)
                        trending.push(e)
                    // }
                }
            }
            if (((e.sponsered == "Yes"))) {
                console.log("🚀 ~ file: contest.js ~ line 247 ~ r.map ~ e", e)
                let p = await usercontestResult.find({ contestId: e._id, userId: req.user._id })
                if (p.length <= 0) {
                    console.log("🚀 ~ file: contest.js ~ line 249 ~ r.map ~ p.length ", p.length)
                    sponsered.push(e)
                }
            }

        })
        let rd = await contest.find({ players: req.user._id, status: "Finished" })
        rd.map(async (e) => {
            let h = []
            let conId = e._id
            let winnerBoard = e.winnerBoard
            let s = await comparedatentime(e.enddate)
            if (s == 0) {
                let g = e.userId.includes(req.user._id)
                if (g == true) {

                    let con = await usercontestResult.find({ contestId: conId })
                    if (con.length > 0) {
                        con.sort(function (a, b) {
                            var keyA = (a.score),
                                keyB = (b.score);
                            if (keyA < keyB) return -1;
                            if (keyA > keyB) return 1;
                            return 0;
                        });

                        let rank = await rankmaker(con, winnerBoard)
                        if (rank) {
                            await contest.findOneAndUpdate({ _id: conId }, { status: "Ended" }, { new: true })

                            winner.create({
                                contestId: conId,
                                rank: rank

                            })

                        }

                    }
                }



            }
        })
        return res.status(200).send({ exam, Trivia, trending, cq, pq, sponsered })
    }
})
exports.getsplash = asyncHandler(async (req, res, next) => {
    let m = {
        "contestImg": "https://res.cloudinary.com/cootzs/image/upload/v1634115466/ticket1.1a5a267a_x7hkcw.png"
    }
    return res.status(200).send(m)
})

exports.market = asyncHandler(async (req, res, next) => {
    let market = [];
    let userid = req.user._id;
    // players: { $nin: userid } 
    let r = await contest.find({ sellType: true, players: { $nin: userid } })
    console.log("r of market ", r)
    console.log(moment().format('DD-MM-YYYY hh:mm'))
    const currentDate = moment().format()
    if (r != null) {
        r.map((e) => {
            let mindiff = moment(e.startdate, 'DD-MM-YYYY hh:mm').diff(moment(currentDate, 'DD-MM-YYYY hh:mm'), 'minutes');
            console.log("mindiff", mindiff)
            if (e.status === "Live" || e.status === "Onapp" && Math.sign(mindiff) == 1 && mindiff >= 10) {
                market.push(e)
            }
        })
        return res.status(200).send(market)
    }
})
exports.pocket = asyncHandler(async (req, res, next) => {
    let market = [];
    let userId = req.user._id;
    console.log(userId)
    let r = await contest.find({ sellType: true, players: userId })
    if (r != null) {
        r.map((e) => {
            if (e.status === "Onapp" || e.status == "Live") {
                market.push(e)
            }
        })
        let orders = await usercontest.find({ userId: req.user._id }).populate('contestId')
        return res.status(200).send({ market, orders })
    }
})
exports.buynsell = asyncHandler(async (req, res, next) => {
    let contestId = req.body.contestId;
    let userId = req.user._id;

    let winningCash = 0;
    let r = await contest.findById(contestId)
    let amount = r['entryFee']
    let totalactiveplayer = (r.players).length
    let totalplayer = r.totalPlayers
    let per = totalactiveplayer / totalplayer
    console.log("per", per)
    if (r != null && per > .40) {

        let s = r.players.indexOf(userId)
        // let pay = await payment(userId, amount, contestId)
        // let paymentId = "vbjnkcgvhbjnkfhgbn"
        // decreasewinningamount(contestId)
        if (s > -1) {
            console.log("s of buynsell", s)
            let newplayer = r.players.splice(s, 1)
            await contest.findOneAndUpdate({ _id: contestId }, { players: newplayer }, { new: true })
            decreasewinningamount(contestId)
            let w = wallet.findOne({ userId: userId });
            winningCash = winningCash + w["winningCash"]
            wallet.findOneAndUpdate({ userId: userId }, { winningCash: winningCash }, { new: true })

            await usercontest.create({
                amount: amount,
                contestId: contestId,
                userId: userId
                // not adding payment Id as of now 
            })
            return res.status(200).send("SOlD")
        }
    } else {
        return res.status(501).send("you can not sell now")
    }
})

exports.userresult = asyncHandler(async (req, res, next) => {
    let useresponse = req.body.userresponse;
    let contestId = req.body.contestId;
    let userId = req.user._id;
    let score = req.body.score;
    let total = 0;
    await useresponse.map((e) => {
        if (e['SelectedAnswer'] == e['correntAnswer']) {
            if (e['Cap'] == "Yes") {
                total = total + 2
            } else if (e['Vc'] == "No") {
                total = total + 1.5
            } else {
                total = total + 1
            }

        } else {
            if (e['Cap'] == "Yes") {
                total = total - 2
            } else if (e['Vc'] == "No") {
                total = total - 1.5
            } else {
                total = total - 1
            }

        }
    })
    await usercontestResult.create({
        userresponse: useresponse,
        userId: userId,
        contestId: contestId,
        score: total
    })

    return res.status(200).send(total)
})
exports.result = asyncHandler(async (req, res, next) => {
    let r = await contest.find({ players: req.user._id, status: "Finished" })
    r.map(async (e) => {
        let h = []
        let conId = e._id
        let winnerBoard = e.winnerBoard
        let s = await comparedatentime(e.enddate)
        if (s == 0) {
            let g = e.userId.includes(req.user._id)
            if (g == true) {

                let con = await usercontestResult.find({ contestId: conId })
                if (con.length > 0) {
                    con.sort(function (a, b) {
                        var keyA = (a.score),
                            keyB = (b.score);
                        if (keyA < keyB) return -1;
                        if (keyA > keyB) return 1;
                        return 0;
                    });

                    let rank = await rankmaker(con, winnerBoard)
                    if (rank) {
                        await contest.findOneAndUpdate({ _id: conId }, { status: "Ended" }, { new: true })

                        winner.create({
                            contestId: conId,
                            rank: rank

                        })

                    }

                }
            }



        }
    })

})
exports.winnerlist = asyncHandler(async (req, res, next) => {
    let userId = req.user._id;

    let final = []

    let c = await contest.find({ players: userId, status: "Finished" })

    c.map(async (e) => {
        let contestId = e._id
        let r = await winner.find({ contestId: contestId })
        if (r.length > 0) {
            final.push[ r[ 0 ] ]
        }

    })
    return res.status(200).send(final)




})


exports.allcontest = asyncHandler(async (req, res, next) => {
    let final = [];
    let b = false

    console.log("userId", req.user._id)
    let r = await contest.find({ "status": "Onapp" }).populate('players', 'username photo')
    if (r) {
        r.map(async (s) => {
            console.log("inside of s")
            await s.players.map((e) => {
                if (b = false && (!e["_id"] == req.user._id)) {
                    console.log("inside of fals eloop")
                    b = true
                    final.push(s)

                }
            })
        })
        res.status(200).send(r)
    }
})
exports.getallcontests = asyncHandler(async (req, res, next) => {
    let final = [];
    let b = false

    // console.log("userId", req.user._id)
    let r = await contest.find({ "status": "Onapp" }).populate('players', 'username photo')
    if (r) {
        res.status(200).send(r)
    }
})
exports.editcontest = asyncHandler(async (req, res, next) => {

    let contestId = req.body.contestId
    let winnerBoard = req.body.winnerBoard

    let s = await contest.findOneAndUpdate({ "_id": contestId }, { winnerBoard: winnerBoard }, { new: true })
    if (s) {
        return res.status(200).send("winnerboard")
    }
})
exports.getcontestquestion = asyncHandler(async (req, res, next) => {

    let contestId = req.params.contestId;
    let question = []

    let s = await contest.findById(contestId).populate('questions');

    if (s != null) {

        // console.log(s)
        let QnaArr = []
        let totaltime = s.TotalTimeInMinute
        let totalquestion = s.totalquestion
        let TotalTimeInMinute = s.TotalTimeInMinute
        // console.log("cfvgbhjnkl", s)

        return res.status(200).send(s);
    }

})

exports.userscontest = asyncHandler(async (req, res, next) => {
    let userId = req.user._id
    console.log("userId", userId);
    let s = await contest.find({ players: userId })
    console.log("s", s)
    res.status(200).send(s)
})
exports.withdrawal = asyncHandler(async (req, res, next) => {
    let userId = req.user._id
    let amount = req.params.amount

    let w = await wallet.findOne({ userId: userId })
    let winningCash = w['winningCash']

    winningCash = winningCash - parseInt(amount)

    let r = await wallet.findOneAndUpdate({ userId: userId }, { winningCash: winningCash }, { new: true })

    return res.status(200).send(r)

})
exports.wallets = asyncHandler(async (req, res, next) => {
    let r = await wallet.findOne({ userId: req.user._id })
    return res.status(200).send(r)
})
exports.updatewallets = asyncHandler(async (req, res, next) => {

    let depositCash = parseInt(req.body.depositcash)
    console.log("🚀 ~ file: contest.js ~ line 477 ~ exports.updatewallets ~ req.body.depositcash", typeof req.body.depositcash)
    let s = await wallet.findOne({ userId: req.user._id })
    if (s?.depositCash != NaN && s?.depositCash != null && s?.depositCash != undefined) {
        console.log("🚀 ~ 480 ", typeof s?.depositCash)
        depositCash += parseInt(s.depositCash);
    }
    let r = await wallet.findOneAndUpdate({ userId: req.user._id }, { depositCash: depositCash })
    return res.status(200).send({ "msg": "Updated" })
})

/*-----------------------------------------------------------Functions----------------------------------------------------------------------------------------------------------------------*/

async function payment(userId, amount, contestId) {
    return true;
}

async function changeentryfee(contestId) {

    let s = await contest.findById(contestId)
    // console.log("s", s)
    let totalactiveplayer = s.players.length
    let totalplayer = s.totalPlayers
    let selltype = s.sellType
    let intialentryFeeentryfee = s.intialentryFee
    let wm = s.intialwinningamount
    let winningmoney = s.winningamount

    let per = totalactiveplayer / totalplayer
    console.log("per", per);
    if ((selltype === true)) {

        if (per > 0.1 && ((per < 0.2) || (per == 0.2))) {
            intialentryFeeentryfee = intialentryFeeentryfee + (intialentryFeeentryfee * .2)

            let d = await contest.findOneAndUpdate({ "_id": contestId }, { entryFee: intialentryFeeentryfee }, { new: true })
        }
        if (per > 0.2 && ((per < 0.3) || (per == 0.3))) {
            intialentryFeeentryfee = intialentryFeeentryfee + (intialentryFeeentryfee * .4)
            let d = await contest.findOneAndUpdate({ "_id": contestId }, { entryFee: intialentryFeeentryfee }, { new: true })
        }
        if (per > 0.3 && ((per < 0.4) || (per == 0.4))) {
            intialentryFeeentryfee = intialentryFeeentryfee + (intialentryFeeentryfee * .6)
            let d = await contest.findOneAndUpdate({ "_id": contestId }, { entryFee: intialentryFeeentryfee }, { new: true })
        }
        if (per > 0.4 && ((per < 0.5) || (per == 0.5))) {
            intialentryFeeentryfee = intialentryFeeentryfee + (intialentryFeeentryfee * .8)

            let d = await contest.findOneAndUpdate({ "_id": contestId }, { entryFee: intialentryFeeentryfee }, { new: true })
        }
        else if (per > 0.5 && ((per < 0.61) || (per == 0.6))) {
            intialentryFeeentryfee = intialentryFeeentryfee + (intialentryFeeentryfee)
            wm = wm + (.5 * (intialentryFeeentryfee) * .1 * totalplayer)
            let d = await contest.findOneAndUpdate({ "_id": contestId }, { entryFee: intialentryFeeentryfee, winningamount: wm }, { new: true })
        }
        else if (per > 0.6 && per < .71) {
            intialentryFeeentryfee = intialentryFeeentryfee + (intialentryFeeentryfee * 1.2)
            winningmoney = winningmoney + (.5 * (intialentryFeeentryfee) * .1 * totalplayer)
            let d = await contest.findOneAndUpdate({ "_id": contestId }, { entryFee: intialentryFeeentryfee, winningamount: winningmoney }, { new: true })
        }
        else if (per > 0.7 && per < .81) {
            intialentryFeeentryfee = intialentryFeeentryfee + (intialentryFeeentryfee * 1.4)
            winningmoney = winningmoney + (.5 * (intialentryFeeentryfee) * .1 * totalplayer)
            let d = await contest.findOneAndUpdate({ "_id": contestId }, { entryFee: intialentryFeeentryfee, winningamount: winningmoney }, { new: true })
        } else if (per > 0.8 && per < .91) {
            intialentryFeeentryfee = intialentryFeeentryfee + (intialentryFeeentryfee * 1.6)
            winningmoney = winningmoney + (.5 * (intialentryFeeentryfee) * .1 * totalplayer)
            let d = await contest.findOneAndUpdate({ "_id": contestId }, { entryFee: intialentryFeeentryfee, winningamount: winningmoney }, { new: true })
        } else if (per > 0.9 && per < .101) {
            intialentryFeeentryfee = intialentryFeeentryfee + (intialentryFeeentryfee * 1.8)
            winningmoney = winningmoney + (.5 * (intialentryFeeentryfee) * .1 * totalplayer)
            let d = await contest.findOneAndUpdate({ "_id": contestId }, { entryFee: intialentryFeeentryfee, winningamount: winningmoney }, { new: true })
        }

    } else {
        return intialentryFeeentryfee
    }



}
async function decreasewinningamount(contestId) {

    let s = await contest.findById(contestId)

    let totalactiveplayer = (s.players).length
    let totalplayer = s.totalPlayers
    let selltype = s.sellType
    let winningmoney = s.winningamount
    let winningamountint = s.intialwinningamount
    let intialentryFeeentryfee = s.intialentryFee
    let entryfee = s.entryfee

    let per = totalactiveplayer / totalplayer
    console.log("per", per);
    if ((selltype === true)) {


        if (per > 0.5 && ((per < 0.61) || (per == 0.6))) {
            winningmoney = Math.abs(winningmoney - (.5 * (intialentryFeeentryfee) * .1 * totalplayer))
            entryfee = entryfee - (intialentryFeeentryfee)
            let d = await contest.findOneAndUpdate({ "_id": contestId }, { winningamount: winningmoney, entryfee: entryfee }, { new: true })
        }
        else if (per > 0.6 && per < .71) {
            winningmoney = Math.abs(winningmoney - (.5 * (intialentryFeeentryfee) * .1 * totalplayer))
            entryfee = entryfee - (entryfee) * .2
            let d = await contest.findOneAndUpdate({ "_id": contestId }, { winningamount: winningmoney, entryfee: entryfee }, { new: true })
        }
        else if (per > 0.7 && per < .81) {
            winningmoney = Math.abs(winningmoney - (.5 * (intialentryFeeentryfee) * .1 * totalplayer))
            entryfee = entryfee - (entryfee) * .4
            let d = await contest.findOneAndUpdate({ "_id": contestId }, { winningamount: winningmoney, entryfee: entryfee }, { new: true })
        } else if (per > 0.8 && per < .91) {
            winningmoney = Math.abs(winningmoney - (.5 * (intialentryFeeentryfee) * .1 * totalplayer))
            entryfee = entryfee - (entryfee) * .6
            let d = await contest.findOneAndUpdate({ "_id": contestId }, { winningamount: winningmoney, entryfee: entryfee }, { new: true })
        } else if (per > 0.9 && per < .101) {
            winningmoney = Math.abs(winningmoney - (.5 * (intialentryFeeentryfee) * .1 * totalplayer))
            entryfee = entryfee - (entryfee) * .8
            let d = await contest.findOneAndUpdate({ "_id": contestId }, { winningamount: winningmoney, entryfee: entryfee }, { new: true })
        }

    } else {
        return winningamountint
    }



}
async function comparedatentime(date) {
    let l = moment().utcOffset("+05:30").format()
    if (l > date) return 1;
    else if (date < l) return -1;
    else return 0;
}
async function winningamounts(totalPlayers, winningamount) {


    let rank1money = parseInt(.06 * winningamount) // 60 
    let rank2money = parseInt(.05 * winningamount) // 50
    let rank3money = parseInt(.03 * winningamount) // 30
    let range1 = parseInt((.07 * totalPlayers)) // 7  
    let rangerank1money = parseInt(.21 * winningamount) // 210
    let range2 = parseInt(.1 * totalPlayers) // 10 
    let rangerank2money = parseInt(.15 * winningamount) //150
    let range3 = parseInt(.2 * totalPlayers) // 20
    let rangerank3money = parseInt(.2 * winningamount) // 200
    let range4 = parseInt(.3 * totalPlayers) // 30
    let rangerank4money = parseInt(.1 * winningamount) // 100

    let range1rank = 3 + range1  // {4,10}
    let range2rank = range1rank + range2 //{11-20}
    let range3rank = range2rank + range3 //{21-40}
    let range4rank = range3rank + range4 // {41-70}

    let winnerBoard = [
        {

            "lowestrange": 0,
            "highestrange": 1,
            "money": parseInt(rank1money)
        },
        {

            "lowestrange": 1,
            "highestrange": 2,
            "money": parseInt(rank2money)

        },
        {
            "lowestrange": 2,
            "highestrange": 3,
            "money": parseInt(rank3money)

        },
        {
            "lowestrange": 4,
            "highestrange": range1rank,
            "money": parseInt(rangerank1money / range1)



        }, {
            "lowestrange": range1rank + 1,
            "highestrange": range2rank,
            "money": parseInt(rangerank2money / range2)



        },
        {
            "lowestrange": range2rank + 1,
            "highestrange": range3rank,
            "money": parseInt(rangerank3money / range3)



        },
        {
            "lowestrange": range3rank + 1,
            "highestrange": range4rank,
            "money": parseInt(rangerank4money / range4)



        },


    ]
    return winnerBoard




}

async function rankmaker(con, winnerBoard) {

    let range1lowestrange = winnerBoard[0]['lowestrange']
    let range1highestrange = winnerBoard[0]['highestrange']
    let range1money = winnerBoard[0]['money']

    let range2lowestrange = winnerBoard[1]['lowestrange']
    let range2highestrange = winnerBoard[1]['highestrange']
    let range2money = winnerBoard[1]['money']

    let range3lowestrange = winnerBoard[2]['lowestrange']
    let range3highestrange = winnerBoard[2]['highestrange']
    let range3money = winnerBoard[2]['money']

    let range4lowestrange = winnerBoard[3]['lowestrange']
    let range4highestrange = winnerBoard[3]['highestrange']
    let range4money = winnerBoard[3]['money']

    let range5lowestrange = winnerBoard[4]['lowestrange']
    let range5highestrange = winnerBoard[4]['highestrange']
    let range5money = winnerBoard[4]['money']

    let range6lowestrange = winnerBoard[5]['lowestrange']
    let range6highestrange = winnerBoard[5]['highestrange']
    let range6money = winnerBoard[5]['money']

    let range7lowestrange = winnerBoard[6]['lowestrange']
    let range7highestrange = winnerBoard[6]['highestrange']
    let range7money = winnerBoard[6]['money']

    await con.map(async (e) => {
        let userId = e.userId[0]
        let user = await user.findById(userId)
        let i = 1;
        let final = []
        if (range1lowestrange < i < range1highestrange) {
            let m = {}
            m.userId = userId
            m.name = user['username']
            m.photo = user['photo']
            m.money = range1money
            m.rank = i
            final.push(m)
            m = {}
            let wa = await wallet.find({ userId: userId })
            let winningCash = winningCash + wa['winningCash']
            await wallet.findOneAndUpdate({ userId: userId }, { winningCash: winningCash }, { new: true })
        }
        if (range2lowestrange < i < range2highestrange) {
            let m = {}
            m.userId = userId
            m.name = user['username']
            m.photo = user['photo']
            m.money = range2money
            m.rank = i
            final.push(m)
            m = {}
            let wa = await wallet.find({ userId: userId })
            let winningCash = winningCash + wa['winningCash']
            await wallet.findOneAndUpdate({ userId: userId }, { winningCash: winningCash }, { new: true })
        }
        if (range3lowestrange < i < range3highestrange) {
            let m = {}
            m.userId = userId
            m.name = user['username']
            m.photo = user['photo']
            m.money = range3money
            m.rank = i
            final.push(m)
            m = {}
            let wa = await wallet.find({ userId: userId })
            let winningCash = winningCash + wa['winningCash']
            await wallet.findOneAndUpdate({ userId: userId }, { winningCash: winningCash }, { new: true })
        }
        if (range4lowestrange < i < range4highestrange) {
            let m = {}
            m.userId = userId
            m.name = user['username']
            m.photo = user['photo']
            m.money = range4money
            m.rank = i
            final.push(m)
            m = {}
            let wa = await wallet.find({ userId: userId })
            let winningCash = winningCash + wa['winningCash']
            await wallet.findOneAndUpdate({ userId: userId }, { winningCash: winningCash }, { new: true })
        }
        if (range5lowestrange < i < range5highestrange) {
            let m = {}
            m.userId = userId
            m.name = user['username']
            m.photo = user['photo']
            m.money = range5money
            m.rank = i
            final.push(m)
            m = {}
            let wa = await wallet.find({ userId: userId })
            let winningCash = winningCash + wa['winningCash']
            await wallet.findOneAndUpdate({ userId: userId }, { winningCash: winningCash }, { new: true })
        }
        if (range6lowestrange < i < range6highestrange) {
            let m = {}
            m.userId = userId
            m.name = user['username']
            m.photo = user['photo']
            m.money = range6money
            m.rank = i
            final.push(m)
            m = {}
            let wa = await wallet.find({ userId: userId })
            let winningCash = winningCash + wa['winningCash']
            await wallet.findOneAndUpdate({ userId: userId }, { winningCash: winningCash }, { new: true })
        }
        if (range7lowestrange < i < range7highestrange) {
            let m = {}
            m.userId = userId
            m.name = user['username']
            m.photo = user['photo']
            m.money = range7money
            m.rank = i
            final.push(m)
            m = {}
            let wa = await wallet.find({ userId: userId })
            let winningCash = winningCash + wa['winningCash']
            await wallet.findOneAndUpdate({ userId: userId }, { winningCash: winningCash }, { new: true })
        }
        i = i + 1

    })
    return final
}