import queryString from query-string;
import env from "..config/environment";
import axios from axios;

import express from express;
const app = express();
const CLIENT_ID= env.development.FB_APP_ID;
const CLIENT_SECRET= env.development.FB_APP_SECRET;
const REDIRECT_URL= env.development.FB_REDIRECT_URL;
const ACCESS_TOKEN= env.development.FB_ACCESS_TOKEN;

// api from controllers before and after login via auth
// app.get("/", (req, res) => {
//   res.send({
//     // edit this api for home page
//   });
// })

// api from controllers after login
// app.get("/fblogin", (req, res) => {
//   res.send({
//     // home page api after auth redirect
//   });
// });

//tbc...

const stringifiedParams = queryString.stringify({
  client_id: CLIENT_ID, //client id of the app (from fb developers)
  redirect_uri: REDIRECT_URL,  
  scope: ["email", "user_friends"].join(","), //user details access-request while loggin in using fb
  response_type: "code",
  auth_type: "rerequest",
  display: "popup",
});

const facebookLoginUrl = `https://www.facebook.com/v4.0/dialog/oauth?${stringifiedParams}`;
console.log(facebookLoginUrl);

async function getAccessTokenFromCode(code) {
  const { data } = await axios({
    url: "https://graph.facebook.com/v4.0/oauth/access_token",
    method: "get",
    params: {
      client_id: CLIENT_ID, //app id from fb developers accnt
      client_secret: CLIENT_SECRET, //app secret from fb developers
      redirect_uri: REDIRECT_URL,
      code,
    },
  });
  console.log("Get access token: " + data); // { access_token, token_type, expires_in }
  return data.access_token;
}

async function getFacebookUserData(access_Token) {
  const { data } = await axios({
    url: "https://graph.facebook.com/me",
    method: "get",
    params: {
      fields: ["id", "email", "first_name", "last_name"].join(","),
      access_token: ACCESS_TOKEN,
    },
  });
  console.log("user data: " + data);
  return data;
}
