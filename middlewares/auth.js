//jwt authentication model
const jwt = require("jsonwebtoken");
const secret = "Mahadev";
const User = require("../models/user");
const ErrorResponse = require('../utils/errorResponse');
const auth = async (req, res, next) => {
  let token;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer')
  ) {
    // Set token from Bearer token in header
    token = req.headers.authorization.split(' ')[1];
    //  console.log('Tokennnnnn', token);
    // Set token from cookie
  }
  // else if (req.cookies.token) {
  //   token = req.cookies.token;
  // }

  // Make sure token exists
  if (!token) {
    return next(new ErrorResponse('Not authorized to access this route', 401));
  }

  try {
    // Verify token
    // console.log("decode",secret);
    const decoded = await jwt.verify(token, secret);
    console.log("decode", decoded);
    req.user = await User.findById(decoded.id).populate('ReferalCode')
    if (!req.user) { return res.status(401).send("not authorized") }
    next();
  } catch (err) {
    console.log(err)
    return res.status(401).send("not authorized");
  }
};

module.exports = auth;
